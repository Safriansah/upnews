package com.blogspot.safriansah.upnews;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.safriansah.upnews.api.APIClient;
import com.blogspot.safriansah.upnews.api.RequestInterface;
import com.blogspot.safriansah.upnews.model.Berita;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class NewsDetailActivity extends AppCompatActivity {

    private String id, link, gambar;
    private TextView judul, tanggal;
    private WebView isi;
    private LinearLayout layout;
    private String strtanggal;
    private SimpleDateFormat formatFrom = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            , formatTo=new SimpleDateFormat("dd MMMM yyyy' | 'HH:mm");
    private Date date;
    private Retrofit retrofit=null;
    private RequestInterface requestInterface=null;
    private SwipeRefreshLayout swiper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        initViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                aksishare();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void aksishare(){
        Picasso.get().load(gambar).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                swiper.setRefreshing(false);
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("*/*");
                i.putExtra(Intent.EXTRA_TEXT, judul.getText().toString()+"\n"+link+"\n#UPNews");
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                startActivity(Intent.createChooser(i, "Gambar"));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                swiper.setRefreshing(false);
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, judul.getText().toString()+"\n"+link+"\n#UPNews");
                startActivity(i);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                swiper.setRefreshing(true);
            }
        });
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private void initViews(){
        id=getIntent().getStringExtra("id");
        judul=findViewById(R.id.news_detail_judul);
        tanggal=findViewById(R.id.news_detail_tanggal);
        isi=findViewById(R.id.news_detail_isi);
        isi.setFocusable(false);
        layout=findViewById(R.id.news_detail_layout);
        swiper=findViewById(R.id.news_detail_refresh);
        swiper.setColorSchemeColors(Color.parseColor("#119085"));
        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadJson();
            }
        });
        layout.setVisibility(View.INVISIBLE);
        loadJson();
    }

    private void loadJson(){
        swiper.setRefreshing(true);
        if(retrofit==null && requestInterface==null){
            retrofit= APIClient.retrofit("http://upnews-api.000webhostapp.com/");
            requestInterface=retrofit.create(RequestInterface.class);
        }
        Call<Berita> call= requestInterface.getPost(id);
        call.enqueue(new Callback<Berita>() {
            @Override
            public void onResponse(Call<Berita> call, Response<Berita> response) {
                try {
                    judul.setText(response.body().getJudul());
                    try {
                        date=formatFrom.parse(response.body().getTanggal());
                        strtanggal=formatTo.format(date);
                        tanggal.setText(strtanggal);
                    }
                    catch (ParseException ex){
                        ex.printStackTrace();
                        tanggal.setText(ex.toString());
                    }
                    isi.loadDataWithBaseURL(null,
                            "<html>" +
                                    "<head><style>img{width:100%;height:auto;margin:12px 0px;}figure{max-width:75%;}</style></head>" +
                                    "<body>"+response.body().getKonten()+"</body>" +
                                    "</html>",
                            "text/html",
                            "utf-8",
                            null);
                    link=response.body().getLink();
                    gambar=response.body().getGambar();
                }
                catch (final Exception ex){
                    Toast.makeText(getApplicationContext(), "Error : "+ex.toString(), Toast.LENGTH_LONG).show();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout.setVisibility(View.VISIBLE);
                        swiper.setRefreshing(false);
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call<Berita> call, Throwable t) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),"Koneksi Gagal", Toast.LENGTH_LONG).show();
                        swiper.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }
}
