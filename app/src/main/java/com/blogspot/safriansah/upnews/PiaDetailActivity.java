package com.blogspot.safriansah.upnews;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.safriansah.upnews.api.APIClient;
import com.blogspot.safriansah.upnews.api.RequestInterface;
import com.blogspot.safriansah.upnews.model.DataPia;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PiaDetailActivity extends AppCompatActivity {

    private String id, txtgambar;
    private TextView judul, tanggal;
    private WebView isi;
    private LinearLayout layout;
    private String strtanggal;
    SimpleDateFormat formatFrom = new SimpleDateFormat("yyyy-MM-dd")
            , formatTo=new SimpleDateFormat("dd MMMM yyyy");
    private Date date;
    private ImageView gambar;
    public SwipeRefreshLayout swiper;
    private Retrofit retrofit=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pia_detail);
        initViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                aksishare();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void aksishare(){
        Picasso.get().load(txtgambar).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                swiper.setRefreshing(false);
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("*/*");
                i.putExtra(Intent.EXTRA_TEXT, judul.getText().toString()+"\n#UPNews #PIA");
                i.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap));
                startActivity(Intent.createChooser(i, "Gambar"));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                swiper.setRefreshing(false);
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_TEXT, judul.getText().toString()+"\n#UPNews #PIA");
                startActivity(i);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                swiper.setRefreshing(true);
            }
        });
    }

    public Uri getLocalBitmapUri(Bitmap bmp) {
        Uri bmpUri = null;
        try {
            File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    private void initViews(){
        id=getIntent().getStringExtra("id");
        judul=findViewById(R.id.pia_detail_judul);
        tanggal=findViewById(R.id.pia_detail_tanggal);
        isi=findViewById(R.id.pia_detail_isi);
        isi.setFocusable(false);
        layout=findViewById(R.id.pia_detail_layout);
        gambar=findViewById(R.id.pia_detail_gambar);
        swiper=findViewById(R.id.pia_detail_refres);
        swiper.setColorSchemeColors(Color.parseColor("#119085"));
        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadJson();
            }
        });
        layout.setVisibility(View.INVISIBLE);
        loadJson();
    }

    private void loadJson(){
        swiper.setRefreshing(true);
        if(retrofit==null){
            retrofit=APIClient.retrofit("https://piatf.000webhostapp.com/");
        }
        RequestInterface requestInterface=retrofit.create(RequestInterface.class);
        Call<DataPia> call= requestInterface.getDataPiaById(id);
        call.enqueue(new Callback<DataPia>() {
            @Override
            public void onResponse(Call<DataPia> call, Response<DataPia> response) {
                try {
                    judul.setText(response.body().getData().get(0).getJudul());
                    try {
                        date=formatFrom.parse(response.body().getData().get(0).getTanggal());
                        strtanggal=formatTo.format(date);
                        tanggal.setText(strtanggal);
                    }
                    catch (ParseException ex){
                        ex.printStackTrace();
                        tanggal.setText(ex.toString());
                    }
                    isi.loadDataWithBaseURL(null,
                            "<html>" +
                                    "<head><style>img{width:100%;height:auto;margin:12px 0px;}figure{max-width:75%;}</style></head>" +
                                    "<body>"+response.body().getData().get(0).getIsi()+"</body>" +
                                    "</html>",
                            "text/html",
                            "utf-8",
                            null);
                    try {
                        Picasso.get()
                                .load("http://piatf.000webhostapp.com/assets/uploads/files/"+response.body().getData().get(0).getGambar())
                                .placeholder(R.drawable.ic_image_green_24dp)
                                .error(R.drawable.ic_error_outline_green_24dp)
                                .into(gambar);
                        txtgambar="http://piatf.000webhostapp.com/assets/uploads/files/"+response.body().getData().get(0).getGambar();
                    }
                    catch (Exception ex){
                        Picasso.get()
                                .load(R.drawable.ic_error_outline_green_24dp)
                                .placeholder(R.drawable.ic_image_green_24dp)
                                .error(R.drawable.ic_error_outline_green_24dp)
                                .into(gambar);
                    }
                }
                catch (final Exception ex){
                    Toast.makeText(getApplicationContext(), "Error : "+ex.toString(), Toast.LENGTH_LONG).show();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        layout.setVisibility(View.VISIBLE);
                        swiper.setRefreshing(false);
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call<DataPia> call, Throwable t) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),"Koneksi Gagal", Toast.LENGTH_LONG).show();
                        swiper.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }

}

