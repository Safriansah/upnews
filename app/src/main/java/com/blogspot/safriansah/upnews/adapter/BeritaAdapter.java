package com.blogspot.safriansah.upnews.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.safriansah.upnews.NewsDetailActivity;
import com.blogspot.safriansah.upnews.R;
import com.blogspot.safriansah.upnews.fragment.TemplateFragment;
import com.blogspot.safriansah.upnews.model.Berita;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BeritaAdapter extends RecyclerView.Adapter {

    private ArrayList<Berita> news;
    private String tanggal;
    SimpleDateFormat formatFrom = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    , formatTo=new SimpleDateFormat("dd MMMM yyyy' | 'HH:mm");
    private Date date;

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    public BeritaAdapter(ArrayList<Berita> news) {
        this.news = news;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_row,parent,false);
            return new ItemViewHolder(view);
        }
        else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_progress_bar,parent,false);
            return new ProgViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return news.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ItemViewHolder){
            if(news.get(position).getJudul().length()>36) ((ItemViewHolder) holder).judul.setText(news.get(position).getJudul().substring(0,36)+"...");
            else  ((ItemViewHolder) holder).judul.setText(news.get(position).getJudul());
            try {
                date=formatFrom.parse(news.get(position).getTanggal());
                tanggal=formatTo.format(date);
                ((ItemViewHolder) holder).tanggal.setText(tanggal);
            }
            catch (ParseException ex){
                ex.printStackTrace();
                ((ItemViewHolder) holder).tanggal.setText(ex.toString());
            }
            try {
                Picasso.get()
                        .load(news.get(position).getGambar())
                        .placeholder(R.drawable.ic_image_green_24dp)
                        .error(R.drawable.ic_error_outline_green_24dp)
                        .fit()
                        .into(((ItemViewHolder) holder).gambar);
            }
            catch (Exception ex){
                Picasso.get()
                        .load(R.drawable.ic_error_outline_green_24dp)
                        .placeholder(R.drawable.ic_image_green_24dp)
                        .error(R.drawable.ic_error_outline_green_24dp)
                        .fit()
                        .into(((ItemViewHolder) holder).gambar);
            }
            ((ItemViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(view.getContext(), NewsDetailActivity.class);
                    i.putExtra("id", news.get(position).getId().toString());
                    view.getContext().startActivity(i);
                }
            });
        }
        else{
            if (position==0){
                ((ProgViewHolder) holder).probar.setVisibility(View.GONE);
                //((ProgViewHolder) holder).bgProBar.setVisibility(View.GONE);
            }
            else{
                ((ProgViewHolder) holder).probar.setVisibility(View.VISIBLE);
                //((ProgViewHolder) holder).bgProBar.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView judul, tanggal, link;
        private ImageView gambar;
        private CardView layout;

        public ItemViewHolder(View itemView) {
            super(itemView);

            layout=itemView.findViewById(R.id.news_row_layout);
            judul=itemView.findViewById(R.id.news_row_judul);
            tanggal=itemView.findViewById(R.id.news_row_tanggal);
            gambar=itemView.findViewById(R.id.news_row_gambar);
            link=itemView.findViewById(R.id.news_row_link);

        }
    }

    public class ProgViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar probar;
        private LinearLayout bgProBar;

        public ProgViewHolder(View itemView) {
            super(itemView);
            probar=itemView.findViewById(R.id.proBar);
            //bgProBar=itemView.findViewById(R.id.bgProBar);
        }
    }
}