package com.blogspot.safriansah.upnews.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.safriansah.upnews.NewsDetailActivity;
import com.blogspot.safriansah.upnews.R;
import com.blogspot.safriansah.upnews.model.Berita;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {

    private ArrayList<Berita> news;
    private String tanggal;
    SimpleDateFormat formatFrom = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    , formatTo=new SimpleDateFormat("dd MMMM yyyy' | 'HH:mm");
    private Date date;

    public NewsAdapter(ArrayList<Berita> news) {
        this.news = news;
    }

    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsAdapter.ViewHolder holder, final int position) {
        if(news.get(position).getJudul().length()>36) holder.judul.setText(news.get(position).getJudul().substring(0,36)+"...");
        else  holder.judul.setText(news.get(position).getJudul());
        try {
            date=formatFrom.parse(news.get(position).getTanggal());
            tanggal=formatTo.format(date);
            holder.tanggal.setText(tanggal);
        }
        catch (ParseException ex){
            ex.printStackTrace();
            holder.tanggal.setText(ex.toString());
        }
        try {
            Picasso.get()
                    .load(news.get(position).getGambar())
                    .placeholder(R.drawable.ic_image_green_24dp)
                    .error(R.drawable.ic_error_outline_green_24dp)
                    .fit()
                    .into(holder.gambar);
        }
        catch (Exception ex){
            Picasso.get()
                    .load(R.drawable.ic_error_outline_green_24dp)
                    .placeholder(R.drawable.ic_image_green_24dp)
                    .error(R.drawable.ic_error_outline_green_24dp)
                    .fit()
                    .into(holder.gambar);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(view.getContext(), NewsDetailActivity.class);
                i.putExtra("id", news.get(position).getId().toString());
                view.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView judul, tanggal, link;
        private ImageView gambar;
        private CardView layout;

        public ViewHolder(View itemView) {
            super(itemView);

            layout=itemView.findViewById(R.id.news_row_layout);
            judul=itemView.findViewById(R.id.news_row_judul);
            tanggal=itemView.findViewById(R.id.news_row_tanggal);
            gambar=itemView.findViewById(R.id.news_row_gambar);
            link=itemView.findViewById(R.id.news_row_link);

        }
    }
}