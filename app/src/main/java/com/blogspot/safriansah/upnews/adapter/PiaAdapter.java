package com.blogspot.safriansah.upnews.adapter;

import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blogspot.safriansah.upnews.PiaDetailActivity;
import com.blogspot.safriansah.upnews.R;
import com.blogspot.safriansah.upnews.model.Pia;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PiaAdapter extends RecyclerView.Adapter{
    private ArrayList<Pia> pia;
    private String tanggal;
    SimpleDateFormat formatFrom = new SimpleDateFormat("yyyy-MM-dd")
            , formatTo=new SimpleDateFormat("dd MMMM yyyy");
    private Date date;

    public PiaAdapter(ArrayList<Pia> pia) {
        this.pia = pia;
    }

    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_row,parent,false);
            return new PiaAdapter.PiaViewHolder(view);
        }
        else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_progress_bar,parent,false);
            return new PiaAdapter.PiaProgViewHolder(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return pia.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof PiaViewHolder){
            if(pia.get(position).getJudul().length()>36) ((PiaViewHolder)holder).judul.setText(pia.get(position).getJudul().substring(0,36)+"...");
            else ((PiaViewHolder)holder).judul.setText(pia.get(position).getJudul());
            try {
                date=formatFrom.parse(pia.get(position).getTanggal());
                tanggal=formatTo.format(date);
                ((PiaViewHolder)holder).tanggal.setText(tanggal);
            }
            catch (ParseException ex){
                ex.printStackTrace();
                ((PiaViewHolder)holder).tanggal.setText(ex.toString());
            }
            try {
                Picasso.get()
                        .load("http://piatf.000webhostapp.com/assets/uploads/files/"+pia.get(position).getGambar())
                        .placeholder(R.drawable.ic_image_green_24dp)
                        .error(R.drawable.ic_error_outline_green_24dp)
                        .fit()
                        .into(((PiaViewHolder)holder).gambar);
            }
            catch (Exception ex){
                Picasso.get()
                        .load(R.drawable.ic_error_outline_green_24dp)
                        .placeholder(R.drawable.ic_image_green_24dp)
                        .error(R.drawable.ic_error_outline_green_24dp)
                        .fit()
                        .into(((PiaViewHolder)holder).gambar);
            }
            ((PiaViewHolder)holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(view.getContext(), PiaDetailActivity.class);
                    i.putExtra("id", pia.get(position).getIdBerita().toString());
                    view.getContext().startActivity(i);
                }
            });
        }
        else{
            ((PiaProgViewHolder) holder).probar.setVisibility(View.GONE);
            //((ProgViewHolder) holder).bgProBar.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return pia.size();
    }

    public class PiaViewHolder extends RecyclerView.ViewHolder {
        private TextView judul, tanggal, link;
        private ImageView gambar;
        private CardView layout;

        public PiaViewHolder(View itemView) {
            super(itemView);

            layout=itemView.findViewById(R.id.news_row_layout);
            judul=itemView.findViewById(R.id.news_row_judul);
            tanggal=itemView.findViewById(R.id.news_row_tanggal);
            gambar=itemView.findViewById(R.id.news_row_gambar);
            link=itemView.findViewById(R.id.news_row_link);

        }
    }

    public class PiaProgViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar probar;
        private LinearLayout bgProBar;

        public PiaProgViewHolder(View itemView) {
            super(itemView);
            probar=itemView.findViewById(R.id.proBar);
            //bgProBar=itemView.findViewById(R.id.bgProBar);
        }
    }
}
