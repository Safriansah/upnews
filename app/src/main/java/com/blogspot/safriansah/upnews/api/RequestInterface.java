package com.blogspot.safriansah.upnews.api;

import com.blogspot.safriansah.upnews.model.Berita;
import com.blogspot.safriansah.upnews.model.DataPia;
import com.blogspot.safriansah.upnews.model.Pia;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RequestInterface {
    //https://upnews-api.000webhostapp.com/
    @GET("index.php/berita")
    Call<ArrayList<Berita>> getAllPost(@Query("halaman") String page);

    @GET("index.php/berita")
    Call<Berita> getPost(@Query("id") String id);

    //https://piatf.000webhostapp.com/
    @GET("index.php/api/id")
    Call<ArrayList<Pia>> getDataPia();

    @POST("index.php/api/berita/id")
    @FormUrlEncoded
    Call<DataPia> getDataPiaById(@Field("id_berita")String id_berita);
}
