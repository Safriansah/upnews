package com.blogspot.safriansah.upnews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

public class AkademikFragment extends TemplateFragment {
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTag();
        initViews(view);
        judul.setText("Akademik");
    }

    private void setTag(){
        tag.add("akademik");
        tag.add("ujian");
    }
}
