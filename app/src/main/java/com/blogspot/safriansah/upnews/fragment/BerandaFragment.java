package com.blogspot.safriansah.upnews.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.safriansah.upnews.R;
import com.blogspot.safriansah.upnews.adapter.NewsAdapter;
import com.blogspot.safriansah.upnews.api.RequestInterface;
import com.blogspot.safriansah.upnews.model.Berita;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BerandaFragment extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<Berita> data=new ArrayList<>();
    private NewsAdapter adapter;
    private RelativeLayout probar;
    private int page;
    private boolean isLoading;
    private ImageButton refresh;
    private TextView more;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_template,  null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view){
        page=1;
        isLoading=false;
        more=view.findViewById(R.id.content_more);
        more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                page++;
                loadJson();
            }
        });
        recyclerView=view.findViewById(R.id.fragment_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager lm= new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(lm);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                final LinearLayoutManager linearLayoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalCount=linearLayoutManager.getItemCount();
                int visCount=linearLayoutManager.getChildCount();
                int firsVisible=linearLayoutManager.findFirstVisibleItemPosition();
                if (!isLoading && (totalCount - visCount) <= firsVisible) {
                    page++;
                    loadJson();
                }
            }
        });
        probar=view.findViewById(R.id.main_progres);
        adapter=new NewsAdapter(data);
        recyclerView.setAdapter(adapter);
        loadJson();
    }

    private void loadJson(){
        probar.setVisibility(View.VISIBLE);
        isLoading=true;
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .build();
        Retrofit retrofit=new Retrofit.Builder()
                //.baseUrl("http://api.learn2crack.com")
                .baseUrl("http://upnews-api.000webhostapp.com/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RequestInterface requestInterface=retrofit.create(RequestInterface.class);
        //Call<JSONResponse> call=requestInterface.getAllPost();
        Call<ArrayList<Berita>> call=requestInterface.getAllPost(String.valueOf(page));
        call.enqueue(new Callback<ArrayList<Berita>>() {
            @Override
            public void onResponse(Call<ArrayList<Berita>> call, Response<ArrayList<Berita>> response) {
                //JSONResponse jsonResponse=response.body();
                try {
                    data.addAll(response.body());
                }
                catch (final Exception ex){
                    Toast.makeText(getContext(), "Error : "+ex.toString(), Toast.LENGTH_LONG).show();
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        probar.setVisibility(View.GONE);
                        isLoading=false;
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call <ArrayList<Berita>> call, Throwable t) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(),"Koneksi Gagal", Toast.LENGTH_LONG).show();
                        probar.setVisibility(View.GONE);
                        isLoading=false;
                        page--;
                        if(page<1) page=1;
                    }
                }, 1000);
            }
        });
    }
}
