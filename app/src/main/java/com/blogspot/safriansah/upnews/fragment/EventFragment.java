package com.blogspot.safriansah.upnews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;


public class EventFragment extends TemplateFragment {
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTag();
        initViews(view);
        judul.setText("Event");
    }

    private void setTag(){
        tag.add("acara");
        tag.add("event");
        tag.add("lomba");
    }
}
