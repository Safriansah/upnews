package com.blogspot.safriansah.upnews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.blogspot.safriansah.upnews.R;

public class LembagaFragment extends TemplateFragment {
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTag();
        initViews(view);
        judul.setText("Lembaga");
    }

    private void setTag(){
        tag.add("lembaga");
        tag.add("rektor");
        tag.add("dekan");
    }
}
