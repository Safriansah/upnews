package com.blogspot.safriansah.upnews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

public class OrmawaFragment extends TemplateFragment {
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTag();
        initViews(view);
        judul.setText("Ormawa");
    }

    private void setTag(){
        tag.add("ormawa");
        tag.add("bem");
        tag.add("hima");
        tag.add("blm");
        tag.add("blj");
    }
}
