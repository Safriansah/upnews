package com.blogspot.safriansah.upnews.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

public class PengumumanFragment extends TemplateFragment {
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTag();
        initViews(view);
        judul.setText("Pengumuman");
    }

    private void setTag(){
        tag.add("pengumuman");
    }
}
