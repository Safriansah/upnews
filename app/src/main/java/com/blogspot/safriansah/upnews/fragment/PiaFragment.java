package com.blogspot.safriansah.upnews.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.safriansah.upnews.R;
import com.blogspot.safriansah.upnews.adapter.PiaAdapter;
import com.blogspot.safriansah.upnews.api.APIClient;
import com.blogspot.safriansah.upnews.api.RequestInterface;
import com.blogspot.safriansah.upnews.model.Pia;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PiaFragment extends Fragment {
    private RecyclerView recyclerView;
    private ArrayList<Pia> data=new ArrayList<>();
    private PiaAdapter adapter;
    private TextView judul;
    public SwipeRefreshLayout swiper;
    private Retrofit retrofit=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_template, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
    }

    private void initViews(View view){
        judul=view.findViewById(R.id.header_judul);
        judul.setText("Pengumuman PIA");
        recyclerView=view.findViewById(R.id.fragment_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager lm= new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(lm);
        adapter=new PiaAdapter(data);
        recyclerView.setAdapter(adapter);
        swiper=view.findViewById(R.id.swiper);
        swiper.setColorSchemeColors(Color.parseColor("#119085"));
        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setRefresh();
            }
        });
        loadJson();
    }

    public void setRefresh(){
        recyclerView.scrollToPosition(0);
        data.clear();
        loadJson();
    }

    private void loadJson(){
        swiper.setRefreshing(true);
        if(retrofit==null){
            retrofit=APIClient.retrofit("https://piatf.000webhostapp.com/");
        }
        RequestInterface requestInterface=retrofit.create(RequestInterface.class);
        Call<ArrayList<Pia>> call=requestInterface.getDataPia();
        call.enqueue(new Callback<ArrayList<Pia>>() {
            @Override
            public void onResponse(Call<ArrayList<Pia>> call, Response<ArrayList<Pia>> response) {
                try {
                    data.addAll(response.body());
                }
                catch (final Exception ex){
                    Toast.makeText(getContext(), "Error : "+ex.toString(), Toast.LENGTH_LONG).show();
                }
                data.add(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        swiper.setRefreshing(false);
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call <ArrayList<Pia>> call, Throwable t) {
                Log.d("tes", "onFailure: "+t.toString());
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(),"Koneksi Gagal", Toast.LENGTH_LONG).show();
                        swiper.setRefreshing(false);
                    }
                }, 1000);
            }
        });
    }
}
