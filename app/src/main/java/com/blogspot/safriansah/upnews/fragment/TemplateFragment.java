package com.blogspot.safriansah.upnews.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.blogspot.safriansah.upnews.R;
import com.blogspot.safriansah.upnews.adapter.BeritaAdapter;
import com.blogspot.safriansah.upnews.adapter.NewsAdapter;
import com.blogspot.safriansah.upnews.api.APIClient;
import com.blogspot.safriansah.upnews.api.RequestInterface;
import com.blogspot.safriansah.upnews.model.Berita;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class TemplateFragment extends Fragment {
    public TextView judul;
    public RecyclerView recyclerView;
    public ArrayList<Berita> data=new ArrayList<>();
    public BeritaAdapter adapter;
    public int page;
    public boolean isLoading;
    public ArrayList<String> tag=new ArrayList<>();
    public SwipeRefreshLayout swiper;
    private Retrofit retrofit=null;
    private RequestInterface requestInterface=null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_template, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void initViews(View view){
        page=1;
        judul=view.findViewById(R.id.header_judul);
        recyclerView=view.findViewById(R.id.fragment_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager lm= new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(lm);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                final LinearLayoutManager linearLayoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalCount=linearLayoutManager.getItemCount();
                int visCount=linearLayoutManager.getChildCount();
                int firsVisible=linearLayoutManager.findFirstVisibleItemPosition();
                if (!isLoading && (totalCount - visCount) <= firsVisible) {
                    loadMore();
                }
            }
        });
        adapter=new BeritaAdapter(data);
        recyclerView.setAdapter(adapter);
        swiper=view.findViewById(R.id.swiper);
        swiper.setColorSchemeColors(Color.parseColor("#119085"));
        swiper.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setRefresh();
            }
        });
        data.add(null);
        swiper.setRefreshing(true);
        loadJson();
    }

    public void setRefresh(){
        recyclerView.scrollToPosition(0);
        data.clear();
        data.add(null);
        page=1;
        swiper.setRefreshing(true);
        loadJson();
    }

    public void loadJson(){
        final int oldJumlah=adapter.getItemCount();
        isLoading=true;
        if(retrofit==null && requestInterface==null){
            retrofit= APIClient.retrofit("http://upnews-api.000webhostapp.com/");
            requestInterface=retrofit.create(RequestInterface.class);
        }
        Call<ArrayList<Berita>> call=requestInterface.getAllPost(String.valueOf(page));
        call.enqueue(new Callback<ArrayList<Berita>>() {
            @Override
            public void onResponse(Call<ArrayList<Berita>> call, Response<ArrayList<Berita>> response) {
                //JSONResponse jsonResponse=response.body();
                ArrayList<Berita> baru=new ArrayList<>();
                try {
                    baru.addAll(response.body());
                    data.remove(data.size()-1);
                }
                catch (final Exception ex){
                    Toast.makeText(getContext(), "Error : "+ex.toString(), Toast.LENGTH_LONG).show();
                    page--;
                    return;
                }
                for(int i=0; i < baru.size(); i++) {
                    if(seleksi(baru.get(i))){
                        data.add(baru.get(i));
                    }
                }
                data.add(null);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        isLoading=false;
                        if(adapter.getItemCount()<=oldJumlah || adapter.getItemCount()<4){
                            if(adapter.getItemCount()>=2) swiper.setRefreshing(false);
                            page++;
                            loadJson();
                        }
                        else{
                            swiper.setRefreshing(false);
                        }
                    }
                }, 1000);
            }

            @Override
            public void onFailure(Call <ArrayList<Berita>> call, Throwable t) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(),"Koneksi Gagal", Toast.LENGTH_LONG).show();
                        swiper.setRefreshing(false);
                        isLoading=false;
                        page--;
                        if(page<1) page=1;
                    }
                }, 1000);
            }
        });
    }

    public boolean seleksi(Berita data){
        if (tag.isEmpty()) return true;
        boolean res=false;
        for(int i=0; i < tag.size(); i++) {
            if(data.getJudul().toLowerCase().contains(tag.get(i)) ||
                    data.getKonten().toLowerCase().contains(tag.get(i))){
                res=true;
                break;
            }
        }
        return res;
    }

    public void loadMore(){
        if(!isLoading){
            page++;
            loadJson();
        }
    }
}
